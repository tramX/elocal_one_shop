import io
from fastapi import APIRouter, File, UploadFile, HTTPException, Depends
from starlette.responses import StreamingResponse
import time
from db.exceptions import NotFoundError


from db.motor.file import IFileDAO
from domain.utils import instance
from domain.utils import create_id
from api_v1.services.auth_service.endpoints import fastapi_users

file_router = APIRouter()
file_dao = instance(IFileDAO)



@file_router.get('/get/{id_}/{file_name}', name='get_file')
async def file(
        id_: str,
        file_name: str = 'blank.jpg'
) -> StreamingResponse:
    try:
        file_ = await file_dao.get(id_)
    except NotFoundError:
        raise HTTPException(status_code=404, detail="Item not found")

    etag = 'file-%s-%s-%s-%s' % (time.mktime(file_.uploadDate.timetuple()),
                                 file_.chunk_size, file_.filename, id_)

    return StreamingResponse(io.BytesIO(await file_.read()), media_type=file_.metadata.get('contentType'),
                             headers={'ETag': etag, 'Last-Modified': str(file_.uploadDate), 'Accept-Ranges': 'bytes',
                                      'Content-Length': str(file_.length)})


@file_router.post('/upload-image')
async def upload_image(title: str, user=Depends(fastapi_users.get_current_user),
                       file: UploadFile = File(...)) -> str:

    if user.is_superuser != True:
        raise HTTPException(status_code=404, detail="Not Access")

    file_dao = instance(IFileDAO)

    id_ = await file_dao.add(file)

    return id_

from fastapi import APIRouter, Depends
from pydantic import BaseModel, UUID4
from typing import Optional

from domain.interfaces import ICategoryDAO
from domain.utils import instance
from domain.entities import ListResponse
from db.category.models import CategoryDB
from api_v1.services.auth_service.endpoints import fastapi_users

router = APIRouter()

HISTORY_PAGE_SIZE = 50

dao = instance(ICategoryDAO)


@router.get('/categories')
async def categories(page: int = 0, category_id: UUID4 = None) -> ListResponse:
    skip = page * HISTORY_PAGE_SIZE

    filters = dict(show=True)

    if category_id and category_id != 'null':
        filters.update({'master_category_id': category_id})
    else:
        filters.update({'master_category_id': None})

    categories_list = await dao.list(skip, HISTORY_PAGE_SIZE, filters)
    # categories_list = await dao.list(skip, HISTORY_PAGE_SIZE, {})

    if categories_list.count == 0:
        categories_list = await dao.list(skip, HISTORY_PAGE_SIZE, {'master_category_id': None})

        return ListResponse(items=categories_list.items, count=categories_list.count)
    return ListResponse(items=categories_list.items, count=categories_list.count)


class AddCategoryRq(BaseModel):
    title: str
    master_category_id: Optional[UUID4]
    description: str
    adult: bool = False


@router.post('/add-category')
async def add_category(rq: AddCategoryRq, user=Depends(fastapi_users.get_current_user)) -> str:
    if user.is_superuser:
        return await dao.create(CategoryDB(**rq.dict()))


@router.delete('/del-category')
async def delete_category(id: UUID4, user=Depends(fastapi_users.get_current_user)):
    if user.is_superuser:
        await dao.delete(id)


class CategoryDetailResp(CategoryDB):
    master_cat_title: Optional[str]


@router.get('/one-category')
async def one_category(category_id: UUID4) -> CategoryDB:
    category: CategoryDB = await dao.get(category_id)
    if category.master_category_id:
        master_caterory: CategoryDB = await dao.get(category.master_category_id)
        return CategoryDetailResp(**category.dict(), master_cat_title=master_caterory.title)
    else:
        return CategoryDetailResp(**category.dict())


class CaregoryUpdate(CategoryDB):
    id: UUID4


@router.post('/update-category')
async def update_category(category: CategoryDB, user=Depends(fastapi_users.get_current_user)) -> bool:
    if user.is_superuser:
        if (category.id == category.master_category_id):
            category.master_category_id = None
        await dao.update(category)
        return True

from pydantic import UUID4, AnyHttpUrl
from typing import Optional
from datetime import datetime

from db.base_model import BaseDBModel


class PromotionDB(BaseDBModel):
    title: str
    description: str
    image_id: str
    add_user_id: Optional[UUID4]
    url: AnyHttpUrl
    show: bool = True
    date_start: datetime
    date_end: datetime
    does_not_end: bool

from pydantic import BaseModel
from typing import Optional, List, Dict, Any
from uuid import UUID


class ImgEntity(BaseModel):
    id: UUID
    title: Optional[str]
    file_id: str
    filename: str
    user_id: Optional[UUID]
    approved: bool = False


class ListResponse(BaseModel):
    items: List[BaseModel]
    count: int


class NotifiationEntity(BaseModel):
    id: UUID
    recipient: Optional[UUID]
    payload: Dict[str, Any]
    shown: bool = False


class NewEntity(BaseModel):
    title: str
    short_description: str
    description: str
    product_url: Optional[str]
    show: bool
    adult: bool = False

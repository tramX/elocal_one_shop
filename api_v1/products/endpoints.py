from fastapi import APIRouter, Depends
from pydantic import BaseModel, UUID4
from typing import Optional, List
from decimal import Decimal
from datetime import datetime

from domain.interfaces import IProductDAO, ICategoryDAO
from domain.utils import instance
from domain.entities import ListResponse
from db.products.models import ProductDB
from api_v1.services.auth_service.endpoints import fastapi_users

router = APIRouter()

HISTORY_PAGE_SIZE = 20

dao = instance(IProductDAO)
category_dao = instance(ICategoryDAO)

class ProductItemResp(BaseModel):
    id: UUID4
    title: str
    description: str
    add_user_id: Optional[UUID4]
    price: Decimal
    old_price: Optional[Decimal]
    category_id: UUID4
    category_title: str
    created_at: datetime
    images: List[str] = []
    show_to_index: bool = False
    last_update: datetime
    quantity: int
    out_of_production: bool


@router.get('/products')
async def products(page: int = 0, category_id: UUID4 = None) -> ListResponse:
    if page > 0:
        page = page - 1
    skip = page * HISTORY_PAGE_SIZE

    filters = dict(show=True, approved=True)

    print('category_id')
    print(category_id)
    print('category_id')

    if category_id:
        filters.update({'category_id': category_id})


    products = await dao.list(skip, HISTORY_PAGE_SIZE, filters)

    if products.count == 0 and category_id:
        categories = await category_dao.list(0, 10, dict(master_category_id=category_id))
        filters.update({'category_id': {'$in': [cat.id for cat in categories.items]}})
        skip = 0
        products = await dao.list(skip, HISTORY_PAGE_SIZE, filters)

    items = [ProductItemResp(**product.dict(), category_title=(await category_dao.get(product.category_id)).title) for product in products.items]

    return ListResponse(items=items, count=products.count)


class AddProductRq(BaseModel):
    title: str
    description: str
    price: Decimal
    old_price: Optional[Decimal]
    category_id: UUID4
    images: List[str] = []
    show_to_index: bool
    quantity: int = 1


@router.post('/add-product')
async def add_product(rq: AddProductRq, user=Depends(fastapi_users.get_current_user)) -> str:
    if user.is_superuser:
        return await dao.create(ProductDB(**rq.dict(), add_user_id=user.id))


@router.delete('/del-product')
async def delete_product(id: UUID4, user=Depends(fastapi_users.get_current_user)):
    if user.is_superuser:
        product: ProductDB = await dao.get(id)
        product.show = False
        await dao.update(product)

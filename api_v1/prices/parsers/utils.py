import time
from pydantic import BaseModel
from chromedriver_py import binary_path
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from config import general_config

options = Options()
options.headless = True

class ProductResp(BaseModel):
    image_url: str
    desc: str


def get_product_image_url_and_desc(product_page_url: str):
    print('###########################################')
    print(general_config.CHROME_BINARY_PATH)
    print('###########################################')

    # if general_config.CHROME_BINARY_PATH:
    #    browser = webdriver.Chrome(executable_path=general_config.CHROME_BINARY_PATH, chrome_options=options)
    # else:
    browser = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)
    # browser = webdriver.Chrome(executable_path='/home/tram/chromedriver', chrome_options=options)

    browser.get(product_page_url)
    time.sleep(3)
    product_img = browser.find_element_by_class_name('no-border')
    product_desc = browser.find_element_by_id('content_description')
    image_url = product_img.get_attribute('src')
    product_resp = ProductResp(image_url=image_url, desc=product_desc.text)
    browser.close()
    return product_resp

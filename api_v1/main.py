import inject
from starlette.middleware.cors import CORSMiddleware

from . import app

from api_v1.injections import base

from api_v1.services.auth_service.endpoints import fastapi_users, jwt_authentication

inject.configure(base)

origins = [
    'http://localhost',
    'http://localhost:8080',
    'http://192.168.4.4:8080',
    'http://localhost:4200',
    'https://cyberland.site',
    'https://admin.cyberland.site',
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


def configure_routers():
    app.include_router(
        fastapi_users.get_register_router(),
        prefix="/auth",
        tags=["auth"],
    )
    app.include_router(
        fastapi_users.get_auth_router(jwt_authentication),
        prefix="/auth/jwt",
        tags=["auth"],
    )

    app.include_router(
        fastapi_users.get_reset_password_router("SECRET"),
        prefix="/auth",
        tags=["auth"],
    )

    app.include_router(
        fastapi_users.get_users_router(),
        prefix="/users",
        tags=["users"],
    )

    from api_v1.categories.endpoints import router as category_router
    from api_v1.products.endpoints import router as product_router
    from api_v1.files.endpoints import file_router
    from api_v1.prices.it_planet_endpoints import router as it_planet_router
    from api_v1.promotions.endpoints import router as promotions_router
    from api_v1.basket.endpoints import router as basket_router
    from api_v1.orders.endpoints import router as orders_router

    app.include_router(category_router, prefix='/categories', tags=['categories'])
    app.include_router(product_router, prefix='/products', tags=['products'])
    app.include_router(file_router, prefix='/files', tags=['files'])
    app.include_router(it_planet_router, prefix='/prices', tags=['it_planet'])
    app.include_router(promotions_router, prefix='/promotions', tags=['promotions'])
    app.include_router(basket_router, prefix='/basket', tags=['basket'])
    app.include_router(orders_router, prefix='/orders', tags=['orders'])

configure_routers()

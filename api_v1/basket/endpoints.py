from fastapi import APIRouter, Depends
from pydantic import BaseModel
from pydantic import UUID4

from domain.utils import instance
from domain.entities import ListResponse
from api_v1.services.auth_service.endpoints import fastapi_users
from domain.interfaces import IBasketDAO
from domain.interfaces import IProductDAO
from db.basket.models import ProductInBasketDB
from db.products.models import ProductDB

router = APIRouter()

HISTORY_PAGE_SIZE = 20

basket_dao = instance(IBasketDAO)
product_dao = instance(IProductDAO)


class Req(BaseModel):
    product_id: UUID4
    quantity: int


@router.post('/add-product-in-basket')
async def create_shop(rq: Req, user=Depends(fastapi_users.get_current_user)) -> bool:
    product = await product_dao.get(rq.product_id)

    b_products = await basket_dao.list(0, 1, {'product_id': rq.product_id, 'user_id': user.id})

    if product:
        if len(b_products.items) > 0:
            b_product = b_products.items[0]
            print(b_product)
            b_product.quantity = rq.quantity
            await basket_dao.update(b_product)
        else:
            id_ = await basket_dao.create(
                ProductInBasketDB(product_id=rq.product_id, quantity=rq.quantity, user_id=user.id))
        return True


class ProductInBaskedResp(ProductInBasketDB):
    product: ProductDB


@router.get('/basket-products')
async def basket_products(user=Depends(fastapi_users.get_current_user)) -> ListResponse:
    b_products = await basket_dao.list(0, 1000, {'user_id': user.id})
    return ListResponse(
        items=[ProductInBaskedResp(**item.dict(), product=(await product_dao.get(item.product_id))) for item in
               b_products.items], count=b_products.count)


@router.delete('/delete-from-basket')
async def delete_from_basket(id: UUID4, user=Depends(fastapi_users.get_current_user)):
    b_product = await basket_dao.get(id)

    if b_product.user_id == user.id:
        await basket_dao.delete(b_product.id)


class BasketUpdateRq(BaseModel):
    id_: UUID4
    quantity: int


@router.post('/basket-update')
async def update_basket(rq: BasketUpdateRq, user=Depends(fastapi_users.get_current_user)):
    b_product: ProductInBasketDB = await basket_dao.get(rq.id_)

    if b_product.user_id == user.id:
        b_product.quantity = rq.quantity
        print(b_product)
        await basket_dao.update(b_product)

from domain.interfaces import IPromotionDAO
from .models import PromotionDB
from db.motor.dao import MotorGenericDAO


class PromotionDAO(MotorGenericDAO, IPromotionDAO):
    def __init__(self):
        super().__init__('promotions', PromotionDB)

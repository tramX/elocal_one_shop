from domain.interfaces import IOrderDAO, IOrderEventDAO
from .models import OrderDB, OrderEventDB
from db.motor.dao import MotorGenericDAO


class OrderDAO(MotorGenericDAO, IOrderDAO):
    def __init__(self):
        super().__init__('orders', OrderDB)


class OrderEventDAO(MotorGenericDAO, IOrderEventDAO):
    def __init__(self):
        super().__init__('order_events', OrderEventDB)

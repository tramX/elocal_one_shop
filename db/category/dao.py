from domain.interfaces import ICategoryDAO
from db.category.models import CategoryDB
from db.motor.dao import MotorGenericDAO


class CategoryDAO(MotorGenericDAO, ICategoryDAO):
    def __init__(self):
        super().__init__('categories', CategoryDB)

from fastapi import APIRouter, HTTPException, Depends, Query, BackgroundTasks, Request, File, UploadFile
from pydantic import BaseModel, UUID4
from typing import List, Optional, Dict, Any
from decimal import Decimal, InvalidOperation, ROUND_FLOOR
import aiofiles
import xlrd
import requests
from PIL import Image, UnidentifiedImageError


from domain.utils import create_id, now
from api_v1.services.auth_service.endpoints import fastapi_users
from api_v1.prices.parsers.utils import get_product_image_url_and_desc

from db.products.models import ProductDB, ProductChainDB, ProductToChainDB

from domain.interfaces import IProductDAO, IProductChainDAO, IProductToChainDAO, ICategoryDAO
from db.motor.file import IFileDAO

from domain.utils import instance


category_dao: ICategoryDAO = instance(ICategoryDAO)
file_dao = instance(IFileDAO)
product_dao = instance(IProductDAO)
product_chain_dao = instance(IProductChainDAO)
product_to_chain_dao = instance(IProductToChainDAO)

router = APIRouter()


class ProductRowEntity(BaseModel):
    title: str
    price: Decimal
    url: str


@router.post('/itplanet-upload-price')
async def itplanet_upload_price(file: UploadFile = File(None), user=Depends(fastapi_users.get_current_user)):
    if user.is_superuser == False:
        raise HTTPException(status_code=404, detail='Доступ запрещен')

    async with aiofiles.open('itp1.xls', 'wb') as out_file:
        content = await file.read()
        await out_file.write(content)

        rb = xlrd.open_workbook('itp1.xls')

        sheet = rb.sheet_by_index(0)

        result = dict()
        current_cat: str

        for rownum in range(sheet.nrows):
            row = sheet.row_values(rownum)
            if len(row[0]) == 0 and len(row[1]) > 1:
                # print('IS ROW CAT', row)
                result.update({row[1]: []})
                current_cat = row[1]
                show_product = True
            else:
                products = await product_dao.list(0, 1, dict(title=row[1]))

                try:
                    Decimal(row[2])
                    if products.count == 0 and len(row[4]) > 0:
                        print(row)
                        result.get(current_cat).append(
                            ProductRowEntity(title=row[1], price=Decimal(row[2]), url=row[4]))
                except InvalidOperation:
                    pass
                # print('IS ROW PRODUCT', row)

        for k in result.keys():
            if len(result.get(k)) > 0:
                print(k, '>>>>>>>>>>>', result.get(k))

        # print(sum([len(result.get(k)) for k in result.keys()]))

    for k in result.keys():
        if len(result.get(k)) > 0:
            chain_in_db = await product_chain_dao.list(0, 1, dict(title=k))

            if chain_in_db.count > 0:
                continue

            id_ = await product_chain_dao.create(ProductChainDB(title=k))
            for p in result.get(k):
                print(p.price)
                await product_to_chain_dao.create(
                    ProductToChainDB(chain_id=id_, title=p.title, url=p.url,
                                            price=p.price.quantize(Decimal("1.00"), ROUND_FLOOR)))


class ProductChainResp(BaseModel):
    id: UUID4
    title: str
    category_id: Optional[UUID4]
    start: bool = False
    product_url: str

async def get_product_url_to_chain(chain_id: UUID4):
    p = await product_to_chain_dao.list(0, 1, dict(chain_id=chain_id))
    return p.items[0].url

@router.get('/show-product-chains')
async def show_product_chains(user=Depends(fastapi_users.get_current_user)) -> List[ProductChainDB]:
    if user.is_superuser == False:
        raise HTTPException(status_code=404, detail='Доступ запрещен')
    chain_result = await product_chain_dao.list(0, 100, {})
    return [ProductChainResp(**item.dict(), product_url=(await get_product_url_to_chain(item.id))) for item in chain_result.items]


@router.delete('/del-product-chain')
async def del_product_chain(id: UUID4, user=Depends(fastapi_users.get_current_user)):
    if user.is_superuser == False:
        raise HTTPException(status_code=404, detail='Доступ запрещен')


    one_product = await product_to_chain_dao.list(0, 1, dict(chain_id=id))

    products = await product_to_chain_dao.list(0, one_product.count, dict(chain_id=id))

    for product in products.items:
        await product_to_chain_dao.delete(product.id)

    await product_chain_dao.delete(id)




class UpdateProductChainRq(BaseModel):
    category_id: UUID4
    start: bool
    chain_id: UUID4

@router.post('/update-product-chain')
async def itplanet_upload_price(rq: UpdateProductChainRq, user=Depends(fastapi_users.get_current_user)):
    if user.is_superuser == False:
        raise HTTPException(status_code=404, detail='Доступ запрещен')

    chain: ProductChainDB = await product_chain_dao.get(rq.chain_id)
    chain.category_id = rq.category_id
    chain.start = rq.start
    await product_chain_dao.update(chain)


@router.get('/start-chains')
async def start_chains(user=Depends(fastapi_users.get_current_user)) -> List[ProductChainDB]:
    if user.is_superuser == False:
        raise HTTPException(status_code=404, detail='Доступ запрещен')

    chain_result = await product_chain_dao.list(0, 100, {'start': True})

    for chain in chain_result.items:
        chain_products = await product_to_chain_dao.list(0, 1500, {'chain_id': chain.id})
        for chain_product in chain_products.items:
            product_data = get_product_image_url_and_desc(chain_product.url)

            r = requests.get(product_data.image_url)
            with open('product.jpg', 'wb') as f:
                f.write(r.content)

                try:
                    img = Image.open('product.jpg')
                    img = img.resize((600, 600), Image.ANTIALIAS)
                    img.save('product.jpg')

                    file = UploadFile('product.jpg', open('product.jpg', 'rb'))

                    id_ = await file_dao.add(file)


                    p_id = await product_dao.create(ProductDB(title=chain_product.title, description=product_data.desc,
                                                              add_user_id=user.id,
                                                              price=chain_product.price, approved=True,
                                                              category_id=chain.category_id,
                                                              images=[id_], last_update=now()))
                except UnidentifiedImageError:
                    p_id = await product_dao.create(ProductDB(title=chain_product.title, description=product_data.desc,
                                                          add_user_id=user.id,
                                                          price=chain_product.price, approved=True,
                                                          category_id=chain.category_id,
                                                          images=[], last_update=now()))

                print(p_id)
            await product_to_chain_dao.delete(chain_product.id)

        await product_chain_dao.delete(chain.id)


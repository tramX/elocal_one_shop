from fastapi import HTTPException
from fastapi.security import OAuth2PasswordBearer
from starlette import status
from starlette.requests import Request
from .endpoints import fastapi_users
from dependency_injector import providers
from dependency_injector.containers import DeclarativeContainer

class AuthService:
    def __init__(self):
        self.scheme = OAuth2PasswordBearer('', auto_error=False)

    async def get_current_user(self, request: Request):
        print('get_current_user')
        token = await self.scheme.__call__(request)
        print(token)
        if token:
            user = await fastapi_users.get_current_user(request)
            user.hashed_password = None
            return user
        else:
            status_code: int = status.HTTP_401_UNAUTHORIZED
            raise HTTPException(status_code=status_code)

class Services(DeclarativeContainer):
    auth_service = providers.Singleton(AuthService)

from fastapi import APIRouter, Depends
from pydantic import BaseModel, UUID4, AnyHttpUrl
from typing import Optional
from datetime import datetime

from domain.interfaces import IPromotionDAO
from db.motor.file import IFileDAO
from domain.utils import instance
from domain.entities import ListResponse
from db.promotions.models import PromotionDB
from api_v1.services.auth_service.endpoints import fastapi_users

router = APIRouter()

HISTORY_PAGE_SIZE = 20

dao = instance(IPromotionDAO)
file_dao = instance(IFileDAO)


@router.get('/promotions')
async def promotions() -> ListResponse:
    filters = dict(show=True)

    promotions = await dao.list(0, HISTORY_PAGE_SIZE, filters)

    return ListResponse(items=promotions.items, count=promotions.count)


class AddPromotionRq(BaseModel):
    title: str
    description: str
    image_id: str
    url: AnyHttpUrl
    show: bool = True
    date_start: datetime
    date_end: datetime
    does_not_end: bool


@router.post('/add-promotion')
async def add_promotion(rq: AddPromotionRq, user=Depends(fastapi_users.get_current_user)) -> str:
    if user.is_superuser:
        return await dao.create(PromotionDB(**rq.dict(), add_user_id=user.id))


@router.delete('/del-promotion')
async def delete_promotion(id: UUID4, user=Depends(fastapi_users.get_current_user)):
    if user.is_superuser:
        promotion: PromotionDB = await dao.get(id)
        await file_dao.delete(promotion.image_id)
        await dao.delete(id)

from config import run_backend_api_config

if __name__ == '__main__':
    import uvicorn

    port = run_backend_api_config.PORT
    if run_backend_api_config.DEV:
        uvicorn.run('api_v1.main:app', host=run_backend_api_config.HOST, port=port, reload=True, debug=True,
                    workers=4)
    else:
        uvicorn.run('api_v1.main:app', host=run_backend_api_config.HOST, port=port, reload=False, debug=False)

import inject

from domain.interfaces import (ICategoryDAO, IProductDAO, IProductChainDAO, IProductToChainDAO, IPromotionDAO,
                               IBasketDAO, IOrderDAO, IOrderEventDAO, INotifiationDAO)
from db.category.dao import CategoryDAO

from db.motor.file import IFileDAO, FileDAO

from db.products.dao import ProductDAO, ProductChainDAO, ProductToChainDAO
from db.promotions.dao import PromotionDAO
from db.basket.dao import BasketDAO
from db.orders.dao import OrderDAO, OrderEventDAO
from db.notifications.dao import NotifiationDAO


def base(binder: inject.Binder):
    binder.bind_to_constructor(ICategoryDAO, lambda: CategoryDAO())
    binder.bind_to_constructor(IFileDAO, lambda: FileDAO())
    binder.bind_to_constructor(IProductDAO, lambda: ProductDAO())
    binder.bind_to_constructor(IProductChainDAO, lambda: ProductChainDAO())
    binder.bind_to_constructor(IProductToChainDAO, lambda: ProductToChainDAO())
    binder.bind_to_constructor(IPromotionDAO, lambda: PromotionDAO())
    binder.bind_to_constructor(IBasketDAO, lambda: BasketDAO())
    binder.bind_to_constructor(IOrderDAO, lambda: OrderDAO())
    binder.bind_to_constructor(IOrderEventDAO, lambda: OrderEventDAO())
    binder.bind_to_constructor(INotifiationDAO, lambda: NotifiationDAO())

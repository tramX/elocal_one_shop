from fastapi import APIRouter, Depends, BackgroundTasks
from pydantic import BaseModel, UUID4
from decimal import Decimal
from typing import List
from datetime import datetime

from api_v1.services.auth_service.endpoints import fastapi_users

from domain.interfaces import IOrderDAO, IOrderEventDAO, IBasketDAO, IProductDAO, INotifiationDAO
from db.orders.models import OrderDB, ProductInOrder, OrderEventDB, OrderStatus

from domain.utils import instance, crete_order_notification_admin_send_email
from domain.entities import ListResponse


order_dao = instance(IOrderDAO)
order_event_dao = instance(IOrderEventDAO)
basket_dao = instance(IBasketDAO)
product_dao = instance(IProductDAO)

notifiation_dao: INotifiationDAO = instance(INotifiationDAO)

router = APIRouter()
HISTORY_PAGE_SIZE = 20


class OrderRq(BaseModel):
    phone: str


@router.post('/create-order')
async def create_order(rq: OrderRq, background_tasks: BackgroundTasks,
                       user=Depends(fastapi_users.get_current_user)) -> bool:
    b_products = await basket_dao.all(dict(user_id=user.id))

    order_sum = sum(
        [((await product_dao.get(b_product.product_id)).price * b_product.quantity) for b_product in b_products])
    order = OrderDB(user_id=user.id, phone=rq.phone,
                    products=[ProductInOrder(quantity=b_product.quantity, product_id=b_product.product_id) for b_product
                              in b_products], order_sum=order_sum, paid=False)
    id_ = await order_dao.create(order)

    for b_product in b_products:
        await basket_dao.delete(b_product.id)

    await order_event_dao.create(OrderEventDB(order_id=id_, order_status=OrderStatus.new.value, user_id=user.id))

    background_tasks.add_task(crete_order_notification_admin_send_email)


class ProductInOrderResp(BaseModel):
    title: str
    price: Decimal
    images: List[str]
    quantity: int


class OrderResp(BaseModel):
    id: UUID4
    order_status: str
    created_at: datetime
    order_sum: Decimal
    products: List[ProductInOrderResp]


class ProductResp(BaseModel):
    title: str
    price: Decimal
    images: List[str]


async def get_order_products_resp(order: OrderDB):
    return [ProductInOrderResp(**ProductResp(**(await product_dao.get(product.product_id)).dict()).dict(),
                               quantity=product.quantity) for product in order.products]


@router.get('/orders')
async def orders(page: int = 0, user=Depends(fastapi_users.get_current_user)) -> ListResponse:
    skip = page * HISTORY_PAGE_SIZE
    orders = await order_dao.list(skip, HISTORY_PAGE_SIZE, {'user_id': user.id, })

    return ListResponse(items=[
        OrderResp(id=order.id, order_sum=order.order_sum, order_status=order.order_status, created_at=order.created_at,
                  products=(await get_order_products_resp(order))) for order in orders.items], count=orders.count)


class ChangeStatusRq(BaseModel):
    order_id: UUID4
    order_status: OrderStatus

from typing import TYPE_CHECKING
import inject
from datetime import datetime
from uuid import uuid4
from typing import Type, TypeVar

import smtplib
from email.mime.text import MIMEText
from email.header import Header

from config import email_config

T = TypeVar('T')


def instance(cls: Type[T]) -> T:
    inst: cls = inject.instance(cls)
    if TYPE_CHECKING:
        assert isinstance(inst, cls)
    return inst


def create_id():
    return uuid4()


def now():
    return datetime.utcnow()


async def crete_order_notification_admin_send_email():
    server = smtplib.SMTP(email_config.MAIL_SERVER, email_config.MAIL_PORT)
    server.ehlo()
    server.starttls()
    server.login(email_config.MAIL_USERNAME, email_config.MAIL_PASSWORD)

    msg = MIMEText('Новый заказ', 'plain', 'utf-8')
    msg['Subject'] = Header('Уведомление от CyberLand', 'utf-8')

    server.sendmail(email_config.MAIL_FROM, 'podwar2008@gmail.com', msg.as_string())
    server.close()

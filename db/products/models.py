from pydantic import validator, UUID4, Field
from typing import Dict, List, Optional
from datetime import datetime
from decimal import Decimal

from domain.utils import now

from db.base_model import BaseDBModel


class ProductDB(BaseDBModel):
    title: str
    description: str
    add_user_id: Optional[UUID4]
    price: Decimal
    old_price: Optional[Decimal]
    approved: bool = True
    show: bool = True
    category_id: UUID4
    created_at: datetime = Field(default_factory=now)
    images: List[str] = []
    show_to_index: bool = False
    last_update: Optional[datetime]
    quantity: int = 1
    out_of_production: bool = False


class ProductChainDB(BaseDBModel):
    title: str
    category_id: Optional[UUID4]
    start: bool = False


class ProductToChainDB(BaseDBModel):
    chain_id: UUID4
    title: str
    url: str
    price: Decimal

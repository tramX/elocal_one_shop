import requests
import smtplib
from email.mime.text import MIMEText
from email.header import Header

from domain.interfaces import INotifiationDAO
from domain.utils import instance
from config import general_config, email_config

notification_dao: INotifiationDAO = instance(INotifiationDAO)


async def send_notification_to_telegram(notificaton_id: str):
    notificaton = await notification_dao.get(notificaton_id)
    r = requests.post('{}/send_message'.format(general_config.TELE_BOT_URL), json=notificaton.payload, verify=False)

    if r.status_code == 200:
        notificaton.shown = True
        await notification_dao.update(notificaton)


async def send_notification_to_email(notificaton_id: str):
    notificaton = await notification_dao.get(notificaton_id)
    notificaton.shown = True

    server = smtplib.SMTP(email_config.MAIL_SERVER, email_config.MAIL_PORT)
    server.ehlo()
    server.starttls()
    server.login(email_config.MAIL_USERNAME, email_config.MAIL_PASSWORD)

    msg = MIMEText(notificaton.payload.get('action'), 'plain', 'utf-8')
    msg['Subject'] = Header('Уведомление от Elocal', 'utf-8')

    server.sendmail(email_config.MAIL_FROM, notificaton.payload.get('email'), msg.as_string())
    server.close()

    await notification_dao.update(notificaton)

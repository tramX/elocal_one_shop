from domain.interfaces import INotifiationDAO
from .models import NotifiationDB
from db.motor.dao import MotorGenericDAO


class NotifiationDAO(MotorGenericDAO, INotifiationDAO):
    def __init__(self):
        super().__init__('notifiations', NotifiationDB)

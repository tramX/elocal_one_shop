from abc import abstractmethod, ABCMeta
from pydantic import BaseModel
from typing import List


class IGenericDAO(metaclass=ABCMeta):
    @abstractmethod
    def create(self, model: BaseModel):
        pass

    @abstractmethod
    def get(self, id_: str):
        pass

    @abstractmethod
    def update(self, model: BaseModel):
        pass

    @abstractmethod
    def delete(self, model: BaseModel):
        pass

    @abstractmethod
    def list(self, skip, limit, filters):
        pass

    @abstractmethod
    def all(self):
        pass

    @abstractmethod
    def count_total(self):
        pass


class ResponseResult(BaseModel):
    items: List[BaseModel]
    count: int


class IProductDAO(IGenericDAO):
    pass


class ICategoryDAO(IGenericDAO):
    pass


class IProductChainDAO(IGenericDAO):
    pass


class IProductToChainDAO(IGenericDAO):
    pass


class IPromotionDAO(IGenericDAO):
    pass


class IBasketDAO(IGenericDAO):
    pass


class IOrderDAO(IGenericDAO):
    pass


class IOrderEventDAO(IGenericDAO):
    pass

class INotifiationDAO(IGenericDAO):
    pass
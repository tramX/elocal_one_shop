from domain.interfaces import IProductDAO, IProductChainDAO, IProductToChainDAO
from .models import ProductDB, ProductChainDB, ProductToChainDB
from db.motor.dao import MotorGenericDAO


class ProductDAO(MotorGenericDAO, IProductDAO):
    def __init__(self):
        super().__init__('products', ProductDB)

class ProductChainDAO(MotorGenericDAO, IProductChainDAO):
    def __init__(self):
        super().__init__('chain_will_add_products', ProductChainDB)


class ProductToChainDAO(MotorGenericDAO, IProductToChainDAO):
    def __init__(self):
        super().__init__('will_add_to_chain_products', ProductToChainDB)
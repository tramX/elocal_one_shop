from pydantic import BaseModel, Field, UUID4
from typing import Dict, Optional, List, Any
from datetime import datetime
from enum import Enum
from decimal import Decimal

from db.base_model import BaseDBModel
from domain.utils import now


class OrderStatus(str, Enum):
    new = 'new'
    accepted = 'accepted'
    making = 'making'
    delivering = 'delivering'
    canceled = 'canceled'
    completed = 'completed'


class ProductInOrder(BaseModel):
    product_id: UUID4
    quantity: int


class OrderDB(BaseDBModel):
    user_id: UUID4
    products: List[ProductInOrder]
    order_status: OrderStatus = OrderStatus.new.value
    created_at: datetime = Field(default_factory=now)
    order_sum: Decimal
    phone: str
    paid: bool


class OrderEventDB(BaseDBModel):
    order_id: UUID4
    user_id: UUID4
    order_status: OrderStatus
    created_at: datetime = Field(default_factory=now)
    payload: Dict[str, Any] = {}

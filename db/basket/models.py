from pydantic import UUID4
from typing import Dict, Optional, Any

from db.base_model import BaseDBModel


class ProductInBasketDB(BaseDBModel):
    product_id: UUID4
    quantity: int
    user_id: UUID4

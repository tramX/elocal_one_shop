from pydantic import BaseSettings
from typing import List, Optional


class MongoConfig(BaseSettings):
    URI: str

    class Config:
        env_file = '.env'


class RunConfigBackendAPI(BaseSettings):
    HOST: str = '0.0.0.0'
    PORT: int = 5002
    DEV: bool = True

    class Config:
        env_file = '.env'


class RunConfigAdminAPI(BaseSettings):
    HOST: str = '0.0.0.0'
    PORT: int = 5001

    class Config:
        env_file = '.env'


class SecretConfig(BaseSettings):
    SECRET_KEY: str

    class Config:
        env_file = '.env'



class GeneralConfig(BaseSettings):
    TELE_BOT_URL: str
    CHROME_BINARY_PATH: Optional[str]


    class Config:
        env_file = '.env'


class EmailConfig(BaseSettings):
    MAIL_USERNAME: str
    MAIL_PASSWORD: str
    MAIL_FROM: str
    MAIL_PORT = 587
    MAIL_SERVER: str
    MAIL_TLS = True
    MAIL_SSL = False

    class Config:
        env_file = '.env'



mongo_config = MongoConfig()
run_backend_api_config = RunConfigBackendAPI()
run_admin_api_config = RunConfigAdminAPI()
secret_config = SecretConfig()
general_config = GeneralConfig()
email_config = EmailConfig()

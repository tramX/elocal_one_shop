from typing import Dict, Optional
from pydantic import UUID4

from db.base_model import BaseDBModel


class CategoryDB(BaseDBModel):
    title: str
    master_category_id: Optional[UUID4]
    description: str
    show: bool = True
    adult: bool = False

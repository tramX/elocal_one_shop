from typing import Dict, Any, Optional
from uuid import UUID
from enum import Enum

from db.base_model import BaseDBModel


class Destination(str, Enum):
    telegram = 'telegram'
    web_portal = 'web_portal'
    email = 'email'
    sms = 'sms'


class NotifiationDB(BaseDBModel):
    recipient: Optional[UUID]
    payload: Dict[str, Any]
    shown: bool = False
    destination: Destination

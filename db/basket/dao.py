from domain.interfaces import IBasketDAO
from .models import ProductInBasketDB
from db.motor.dao import MotorGenericDAO


class BasketDAO(MotorGenericDAO, IBasketDAO):
    def __init__(self):
        super().__init__('basket_products', ProductInBasketDB)
